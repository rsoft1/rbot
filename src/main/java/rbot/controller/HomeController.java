package rbot.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import rbot.RBot;
import rbot.model.Contact;
import rbot.model.dao.ContactDao;
import rbot.view.MainView;
import rbot.view.utils.CellRender;
import rbot.view.utils.TableModel;

/**
 *
 * @author RSfot
 */
public class HomeController {

    //Variables de rbot
    public RBot rbot;

    //Ventana principal, Controllador
    private final MainView mainView;
    private final MainController mc;
    private final ConfigController configCtrl;

    private Icon defaultIcon;

    //Variables de datos y persistencoia
    private ContactDao contactDao;
    private List<Contact> contactos;
    private TableModel tableModelContact;

    public HomeController(MainController mc, MainView mv) {
        this.mc = mc;
        this.mainView = mv;
        this.configCtrl = new ConfigController(this.mainView);
    }

    public void init(ContactDao contactDao, List<Contact> contactos) {
        this.config_view();
        this.buttons_events();
        this.load_data(contactDao, contactos);
        initRbot();
    }

    private void config_view() {

        /*Configuramos la tabla de contactos*/
        mainView.tableContacts.getTableHeader().setFont(new Font("Century Gothic", Font.PLAIN, 16));
        mainView.tableContacts.getTableHeader().setOpaque(false);
        mainView.tableContacts.getTableHeader().setBackground(new Color(41, 155, 41));
        mainView.tableContacts.getTableHeader().setForeground(Color.white);

        /*Desactivamos los botones de envio de mensajes hasta que se autentique el usuario*/
        mainView.btnDiffusion.setEnabled(false);
        mainView.btnSend.setEnabled(false);

        /*Guardamos el icono por defecto del codigo QR*/
        defaultIcon = mainView.QR.getIcon();
    }

    private void buttons_events() {
        
        mainView.btnClose.addActionListener((event) -> {
           if(rbot != null){
               rbot.quit();
               System.exit(0);
           } 
        });
        
        mainView.btnDiffusion.addActionListener((event) -> {
            this.sendDiffusion();
        });

        mainView.btnSend.addActionListener((event) -> {
            this.sendMessage();
        });

        mainView.btnAdd.addActionListener((event) -> {
            this.mainView.tableContacts.clearSelection();
            this.showAddContactModal();
        });

        mainView.btnSave.addActionListener((event) -> {
            this.saveContact();
        });

        mainView.btnEdit.addActionListener((event) -> {
            this.showAddContactModalEdit();
        });

        mainView.btnDelete.addActionListener((event) -> {
            this.deleteContact();
        });

        mainView.btnLock.addActionListener((event) -> {
            this.changeStateContact();
        });

        //Eventos de la pantalla de configuracion
        this.configCtrl.buttons_events();
    }

    private void load_data(ContactDao contactDao, List<Contact> contactos) {
        this.contactDao = contactDao;
        setDataTableContacts(contactos);

        //mostramos la ventana de home
        mc.switchPanes("home");
    }

    //  FUNCIONES DE LOS BOTONES
    private void sendDiffusion() {
        int rowsCount = this.mainView.tableContacts.getRowCount();

        this.mainView.btnDiffusion.setEnabled(false);

        //Definimos el retardo entre el envio de los mensajes
        int delayValueConfig = Integer.parseInt(this.configCtrl.getConfig("retardo") + "");
        rbot.setDelayValue(delayValueConfig);

        Thread t1 = new Thread(() -> {

            String mensajePorDefecto = String.valueOf(this.configCtrl.getConfig("mensaje_defecto"));

            for (int i = 0; i < rowsCount; i++) {

                this.mainView.select_row = i;

                Long id = Long.parseLong(tableModelContact.getValueAt(mainView.select_row, 0) + "");

                Contact contact = this.getContactByID(id);

                if (!contact.isDefault_message()) {
                    //enviar mensaje propio
                    mainView.tableContacts.setEnabled(false);
                    mainView.tableContacts.setValueAt("Enviando", mainView.select_row, 1);
                    String response = rbot.sendMessage("+" + contact.getIndicative() + contact.getNumber(), contact.getMessage());

                    if (response.equals("ok")) {
                        mainView.tableContacts.setValueAt("Enviado", mainView.select_row, 1);
                    } else {
                        mainView.tableContacts.setValueAt("Error", mainView.select_row, 1);
                        System.out.println(response);
                    }

                    mainView.select_row = -1;
                    mainView.tableContacts.setEnabled(true);

                } else {
                    //enviar mensaje global
                    //enviar mensaje propio
                    mainView.tableContacts.setEnabled(false);
                    mainView.tableContacts.setValueAt("Enviando", mainView.select_row, 1);

                    String response = rbot.sendMessage("+" + contact.getIndicative() + contact.getNumber(), mensajePorDefecto);

                    if (response.equals("ok")) {
                        mainView.tableContacts.setValueAt("Enviado", mainView.select_row, 1);
                    } else {
                        mainView.tableContacts.setValueAt("Error", mainView.select_row, 1);
                        System.out.println(response);
                    }

                    mainView.select_row = -1;
                    mainView.tableContacts.setEnabled(true);
                }

            }

            this.mainView.btnDiffusion.setEnabled(true);
        });

        t1.start();
    }

    private void sendMessage() {
        if (mainView.select_row != -1) {

            Long id = Long.parseLong(tableModelContact.getValueAt(mainView.select_row, 0) + "");

            Contact contact = this.getContactByID(id);

            //Definimos el retardo entre el envio de los mensajes
            int delayValueConfig = Integer.parseInt(this.configCtrl.getConfig("retardo") + "");
            rbot.setDelayValue(delayValueConfig);

            if (!contact.isDefault_message()) {
                //enviar mensaje propio
                Thread t1 = new Thread(() -> {
                    mainView.tableContacts.setEnabled(false);
                    mainView.tableContacts.setValueAt("Enviando", mainView.select_row, 1);

                    String response = rbot.sendMessage("+" + contact.getIndicative() + contact.getNumber(), contact.getMessage());

                    if (response.equals("ok")) {
                        mainView.tableContacts.setValueAt("Enviado", mainView.select_row, 1);
                    } else {
                        mainView.tableContacts.setValueAt("Error", mainView.select_row, 1);
                        System.out.println(response);
                    }

                    mainView.select_row = -1;
                    mainView.tableContacts.setEnabled(true);
                });
                t1.start();

            } else {
                //enviar mensaje global
                Thread t1 = new Thread(() -> {

                    String mensajePorDefecto = String.valueOf(this.configCtrl.getConfig("mensaje_defecto"));

                    mainView.tableContacts.setEnabled(false);
                    mainView.tableContacts.setValueAt("Enviando", mainView.select_row, 1);

                    String response = rbot.sendMessage("+" + contact.getIndicative() + contact.getNumber(), mensajePorDefecto);

                    if (response.equals("ok")) {
                        mainView.tableContacts.setValueAt("Enviado", mainView.select_row, 1);
                    } else {
                        mainView.tableContacts.setValueAt("Error", mainView.select_row, 1);
                        System.out.println(response);
                    }

                    mainView.select_row = -1;
                    mainView.tableContacts.setEnabled(true);
                });
                t1.start();
            }

        } else {
            JOptionPane.showMessageDialog(null, "Elija un contacto");
        }
    }

    private void showAddContactModal() {
        mainView.addContactDialog.setLocationRelativeTo(null);
        mainView.addContactDialog.setVisible(true);
    }

    private void saveContact() {
        if (mainView.select_row == -1) {
            //Nuevo contacto
            Contact contact = new Contact();

            contact.setIndicative(mainView.txtIndicative.getText());
            contact.setState("Activo");
            contact.setNumber(mainView.txtNumber.getText());
            contact.setMessage(mainView.txtMessage.getText());
            contact.setDefault_message(mainView.checkDefaultMessage.isSelected());

            mainView.txtNumber.setText("");
            mainView.txtMessage.setText("");
            mainView.txtIndicative.setText("");
            mainView.checkDefaultMessage.setSelected(false);

            contactDao.create(contact);

            mainView.addContactDialog.dispose();

            this.setDataTableContacts(null);
        } else {

            Long id = Long.parseLong(tableModelContact.getValueAt(mainView.select_row, 0) + "");

            Contact contact = this.getContactByID(id);

            contact.setIndicative(mainView.txtIndicative.getText());
            contact.setNumber(mainView.txtNumber.getText());
            contact.setMessage(mainView.txtMessage.getText());
            contact.setDefault_message(mainView.checkDefaultMessage.isSelected());

            contactDao.update(contact);

            mainView.addContactDialog.dispose();

            this.setDataTableContacts(null);
        }
    }

    private void showAddContactModalEdit() {
        if (mainView.select_row != -1) {

            Long id = Long.parseLong(tableModelContact.getValueAt(mainView.select_row, 0) + "");

            Contact contact = this.getContactByID(id);

            mainView.txtNumber.setText(contact.getNumber());
            mainView.txtMessage.setText(contact.getMessage());
            mainView.txtIndicative.setText(contact.getIndicative());
            mainView.checkDefaultMessage.setSelected(contact.isDefault_message());

            mainView.addContactDialog.setLocationRelativeTo(null);
            mainView.addContactDialog.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Elija un contacto");
        }
    }

    private void deleteContact() {
        if (mainView.select_row != -1) {

            int dialogResult = JOptionPane.showConfirmDialog(null, "¿Desea borrar este contacto?", "Confirmacion", 2);

            if (dialogResult == JOptionPane.YES_OPTION) {
                Long id = Long.parseLong(tableModelContact.getValueAt(mainView.select_row, 0) + "");
                contactDao.delete(id);
                tableModelContact.removeRow(mainView.select_row);
                mainView.select_row = -1;
            }

        } else {
            JOptionPane.showMessageDialog(null, "Elija un contacto");
        }

    }

    private void changeStateContact() {
        if (mainView.select_row != -1) {

            Long id = Long.parseLong(tableModelContact.getValueAt(mainView.select_row, 0) + "");

            Contact contact = this.getContactByID(id);

            String new_state = "Activo".equals(contact.getState()) ? "Bloqueado" : "Activo";

            contact.setState(new_state);

            contactDao.update(contact);

            this.setDataTableContacts(null);
        } else {
            JOptionPane.showMessageDialog(null, "Elija un contacto");
        }
    }

    // FUNCIONES AUXILIARES
    private Contact getContactByID(Long id) {
        //actualizar contacto
        return contactos.stream()
                .filter((c) -> c.getId() == id)
                .findAny()
                .orElse(null);
    }

    private void setDataTableContacts(List<Contact> contacts) {
        String[] titles = {"Id", "Estado", "Numero", "Mensaje"};

        contactos = contacts == null ? contactDao.read(true, 0, 0) : contacts;

        Object[][] data = new Object[contactos.size()][4];

        for (int i = 0; i < contactos.size(); i++) {
            data[i][0] = contactos.get(i).getId() + "";
            data[i][1] = contactos.get(i).getState();
            data[i][2] = contactos.get(i).getIndicative() + contactos.get(i).getNumber();
            data[i][3] = contactos.get(i).getMessage();
        }

        tableModelContact = new TableModel(titles, data);

        mainView.tableContacts.setModel(tableModelContact);

        for (int i = 0; i < titles.length; i++) {
            mainView.tableContacts.getColumnModel().getColumn(i).setCellRenderer(new CellRender());
        }
    }

    //FUNCIONES RBOT
    private void initRbot() {

        Thread t = new Thread(() -> {
            rbot = new RBot(true);
            this.autenticateRbot();
        });

        t.start();
    }

    private void autenticateRbot() {
        mainView.QR.setIcon(defaultIcon);
        String codeQR = rbot.initialize();
        mainView.QR.setIcon(new ImageIcon(codeQR));
        try {
            String state = rbot.autenticate();
            if ("Online".equals(state)) {
                mainView.labelState.setForeground(Color.GREEN);
                mainView.labelState.setText(state);
                mainView.btnDiffusion.setEnabled(true);
                mainView.btnSend.setEnabled(true);
            }
        } catch (Exception e) {
            rbot.quit();
            this.initRbot();
        }

    }

    public void reloadQr() {
        mainView.QR.setIcon(defaultIcon);
        this.autenticateRbot();
    }

}
