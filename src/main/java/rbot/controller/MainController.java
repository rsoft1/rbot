package rbot.controller;

import rbot.view.MainView;

public class MainController {

    //VENTANA PRINCIPAL
    private final MainView mainView;
    
    //CONTROLADORES VENTANAS SECUNDARIAS (PANELES)
    public final LoginController loginController;
    public final LoaderController loaderController;
    public final HomeController homeController;
   
    public MainController() {
        mainView = new MainView();
        loginController = new LoginController(this, mainView);
        loaderController = new LoaderController(this, mainView);
        homeController = new HomeController(this, mainView);
    }

    public void _init() {
        this._config_view();
        this._buttons_events();
    }

    private void _config_view() {
        mainView.setResizable(false);
        mainView.setLocationRelativeTo(null);
        mainView.setVisible(true);
        
        loginController.init();
    }

    private void _buttons_events() {
        
        mainView.btnHome.addActionListener((event) -> {
            switchPanes("home");
        });
        
        mainView.btnBot.addActionListener((event) -> {
            switchPanes("bot");
        });
        
        mainView.btnReports.addActionListener((event) -> {
            switchPanes("reports");
        });
        
        mainView.btnConfig.addActionListener((event) -> {
            switchPanes("config");
        });
        
        mainView.btnRefresh.addActionListener((e) -> {
            if ("Offline".equals(mainView.labelState.getText())) {
                Thread t1 = new Thread(() -> {
                    homeController.reloadQr();
                });
                t1.start();
            }
        });

        mainView.btnClose.addActionListener((e) -> {
            if (homeController.rbot != null) {
                homeController.rbot.quit();
            }
            mainView.dispose();
            System.exit(0);
        });

    }

    public void switchPanes(String pane) {
        
        mainView.loginPane.setVisible("login".equals(pane));
        mainView.mainPane.setVisible("home".equals(pane));
        mainView.botPane.setVisible("bot".equals(pane));
        mainView.reportsPane.setVisible("reports".equals(pane));
        mainView.configPane.setVisible("config".equals(pane));

        mainView.loaderPane.setVisible("loader".equals(pane));

        mainView.btnHome.setVisible(!"loader".equals(pane) && !"login".equals(pane));
        mainView.btnBot.setVisible(!"loader".equals(pane) && !"login".equals(pane));
        mainView.btnRefresh.setVisible(!"loader".equals(pane) && !"login".equals(pane));
        mainView.btnReports.setVisible(!"loader".equals(pane) && !"login".equals(pane));
        mainView.btnConfig.setVisible(!"loader".equals(pane) && !"login".equals(pane));

    }
    
}
