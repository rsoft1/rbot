package rbot.controller;

import java.util.HashMap;
import javax.swing.JOptionPane;
import rbot.model.Config;
import rbot.model.dao.ConfigDao;
import rbot.view.MainView;

public class ConfigController {

    private final MainView mainView;
    private Config configuration;
    private final ConfigDao configDao;

    public ConfigController(MainView mainView) {
        this.mainView = mainView;
        this.configDao = new ConfigDao();
        this.init();
    }

    public void init() {
        try {
            //cargamos las configuraciones
            this.configuration = this.configDao.get();
            
        } catch (Exception e) {
            //si no existe una configuracion la creamos y guardamos

            this.configuration = new Config(new HashMap<>());

            this.configuration.put("retardo", 5);
            this.configuration.put("mensaje_defecto", "Este es un mensaje por defecto");

            this.configDao.create(this.configuration);
        }

        int retardo = Integer.parseInt(this.configuration.get("retardo") + "");
        String mensaje_defecto = this.configuration.get("mensaje_defecto") + "";

        this.mainView.txtDelay.setValue(retardo);
        this.mainView.txtMessageDefault.setText(mensaje_defecto);
    }

    public void buttons_events() {
        this.mainView.btnSaveConfig.addActionListener(e -> {
            saveConfig();
        });
    }

    public Object getConfig(String name) {
        return this.configuration.get(name);
    }

    private void saveConfig() {
        int retardo = Integer.parseInt(this.mainView.txtDelay.getValue() + "");
        String mensajePorDefecto = this.mainView.txtMessageDefault.getText();

        this.configuration.put("retardo", retardo);
        this.configuration.put("mensaje_defecto", mensajePorDefecto);

        this.configDao.update(this.configuration);
        
        JOptionPane.showMessageDialog(null,"Guardado con exito");
    }

}
