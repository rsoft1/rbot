package rbot.controller;

import java.util.List;
import rbot.model.dao.ContactDao;
import rbot.view.MainView;
import rbot.model.Contact;

public class LoaderController {

    private final MainController mc;
    private final MainView mainView;
    
    private ContactDao contactDao;
    private List<Contact> contacts;

    public LoaderController(MainController mc, MainView mainView) {
        this.mc = mc;
        this.mainView = mainView;
    }
    
    public void init(){
        mc.switchPanes("loader");
        load_data();
    }
    
    private void load_data(){
        labelLoader("Conectando con la base de datos...");
        contactDao = new ContactDao();

        labelLoader("Obteniendo datos...");
        contacts = contactDao.read(true, 0, 0);
        
        mc.homeController.init(contactDao, contacts);
    }

    private void labelLoader(String info) {
        mainView.labelLoader.setText(info);
    }


}
