package rbot.controller;

import rbot.services.LoginService;
import rbot.view.MainView;

public class LoginController {

    //Ventana principal, Controllador principal
    private final MainView mv;
    private final MainController mc;

    public final LoginService ls;

    public LoginController(MainController mc, MainView mv) {
        this.mc = mc;
        this.mv = mv;
        this.ls = new LoginService();
    }

    public void init() {
        this.config_view();
        this.buttons_events();
    }

    private void config_view() {
        mc.switchPanes("login");
        mv.labelErrorRequest.setVisible(false);
        mv.labelSendRequest.setVisible(false);
    }

    private void buttons_events() {

        //LOGIN BUTTON EVENT
        mv.btnIngresar.addActionListener((event) -> {
            this.login();
        });
        
        mv.txtPassword.addActionListener((event) -> {
            this.login();
        });

    }

    private void login() {
        //gif loader view
        mv.labelSendRequest.setVisible(true);
        mv.labelErrorRequest.setVisible(false);

        String username = mv.txtUsername.getText();
        String password = mv.txtPassword.getText();

        if (username.length() > 0 && password.length() > 0) {
            Thread t = new Thread(() -> {
                String response = ls.login(username, password);
                if (response.equals("ok")) {
                    mc.loaderController.init();
                } else {
                    mv.labelErrorRequest.setVisible(true);
                    mv.labelErrorRequest.setText(response);
                    mv.labelSendRequest.setVisible(false);
                }
            });
            t.start();
        }
    }

}
