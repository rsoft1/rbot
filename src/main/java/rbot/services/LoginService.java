package rbot.services;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.json.JSONObject;

public class LoginService {

    final String URL_API = Global.API_URL;

    public String username;
    public String email;
    public String accessToken;
    public String instance;

    public LoginService() {
        username = "";
        email = "";
        accessToken = "";
        instance = "";
    }

    public String login(String username, String password) {
        
        JSONObject body = new JSONObject();
        body.put("username", username);
        body.put("password", password);

        try {
            JsonNode response = Unirest.post(this.URL_API + "login")
                    .header("Content-Type", "application/json")
                    .body(body)
                    .asJson()
                    .getBody();

            int status = (int) response.getObject().get("status");

            
            
            switch (status) {
                case 200:

                    JSONObject data = (JSONObject) response.getObject().get("data");
                    
                    JSONObject instanceObject = (JSONObject) data.getJSONArray("instance").get(0);
                    
                    username = data.get("username").toString();
                    email = data.get("email").toString();
                    accessToken = data.get("accessToken").toString();
                    instance = instanceObject.get("name").toString();

                    return "ok";

                case 409:
                    //return response.getObject().get("msg").toString();
                    return "Credenciales invalidas";
                case 500:
                    //return response.getObject().get("msg").toString();
                    return "Error en el servidor";
            }

            return null;

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }

    }

}
