package rbot.model;

import java.io.Serializable;
import java.util.HashMap;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Config implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    private HashMap<String, Object> hashMap;

    public Config() {
    }

    public Config(HashMap<String, Object> hashMap) {
        this.hashMap = hashMap;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Object get(String key) {
        return this.hashMap.get(key);
    }

    public void put(String key, Object value) {
        this.hashMap.put(key, value);
    }

}
