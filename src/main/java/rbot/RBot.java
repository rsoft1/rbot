package rbot;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ANTABLA
 */
public class RBot {

    /*CONSTANTES DEL DOM DE WHATSAPP*/
    private final String WHATSAPP_URL = "https://web.whatsapp.com/";
    private final String _CANVAS = "canvas";
    private final String _APP = "span[data-icon=\"chat\"]";
    private final String _SEND_BUTTON_MSG = "span[data-icon=\"send\"]";
    private final String _SEND_BUTTON_IMG = "div>span[data-icon=\"send-light\"]";
    private final String _CLIP = "span[data-icon=\"clip\"]";
    private final String _INPUT_FILE = "input[type=\"file\"]";
    private final String _CAPTION_FILE = "div.copyable-text:last-child";
    private final String _MODAL = "div[data-animate-modal-popup=\"true\"]";
    private final String _CHECK_MSG = ".message-out:last-child > div > div > div > div:last-child > div > div > span[data-icon=\"msg-check\"]";
    private final String _DBLCHECK_MSG = ".message-out:last-child > div > div > div > div:last-child > div > div > span[data-icon=\"msg-dblcheck\"]";

    private final String DRIVER_TYPE = "webdriver.chrome.driver";
    private String DRIVER_PATH = "/driver/chromedriver.exe";

    private final WebDriver driver;
    private final WebDriverWait wait;

    private int delayValue;

    public RBot(boolean hide) {

        DRIVER_PATH = getClass().getResource(DRIVER_PATH).getPath();
        System.setProperty(DRIVER_TYPE, DRIVER_PATH);

        ChromeOptions options = new ChromeOptions();

        options.setHeadless(hide);
        options.addArguments("--start-maximized");
        options.addArguments("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36");

        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 30);
    }

    public void setDelayValue(int delay) {
        this.delayValue = delay;
    }

    public String initialize() {
        driver.get(WHATSAPP_URL);
        _delay(3);
        //Esperar hasta que cargue el codigo qr de whatsapp
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(_CANVAS)));
        WebElement QR = driver.findElement(By.cssSelector(_CANVAS));

        return this.getCodeQRImage(QR);
    }

    public String autenticate() {
        //Esperar hasta que se autentique
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(_APP)));
        return "Online";
    }

    /**
     * Envia un mensaje a un numero dado
     *
     * @param number
     * @param message
     * @return
     */
    public String sendMessage(String number, String message) {

        //ENTER GLOBAL
        driver.findElement(By.tagName("html")).sendKeys(Keys.ENTER);

        try {
            driver.get(WHATSAPP_MESSAGE(number, message));

            //comprobamos el numero de telefono
            if (checkNumber()) {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(_SEND_BUTTON_MSG)));
                driver.findElement(By.cssSelector(_SEND_BUTTON_MSG)).click();

                if (checkMessage()) {
                    return "ok";
                } else {
                    return "El mensaje no pudo ser enviado";
                }
            } else {
                return "Numero de telefono invalido";
            }
        } catch (Exception e) {
            return "Ocurrio un error al enviar el mensaje";
        }

    }

    /**
     * Envia un mensaje a un numero dado
     *
     * @param number
     * @param message
     * @param mediaPath
     * @return
     */
    public String sendMessageWithMedia(String number, String message, String mediaPath) {

        try {

            sendMessage(number, message);

            //Damos click en el click para que se muestre el input-file
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(_CLIP)));
            driver.findElement(By.cssSelector(_CLIP)).click();

            _delay(this.delayValue);

            //cargamos el archivo
            WebElement input_file = driver.findElement(By.cssSelector(_INPUT_FILE));
            input_file.sendKeys(mediaPath);

            //enviamos el mensaje
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(_SEND_BUTTON_IMG)));
            driver.findElement(By.cssSelector(_SEND_BUTTON_IMG)).click();

            if (checkMultimedia()) {
                return "Archivo enviado con exito";
            } else {
                return "El Archivo no pudo ser enviado";
            }

        } catch (Exception e) {
            return "Error al enviar el archivo u el mensaje";
        }

    }

    /**
     * Comprueba que el mensaje alla sido enviado correctamente
     */
    private boolean checkMessage() {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(_CHECK_MSG + ", " + _DBLCHECK_MSG)));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Comprueba que el mensaje multimedia alla sido enviado correctamente
     */
    private boolean checkMultimedia() {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(_CHECK_MSG + ", " + _DBLCHECK_MSG)));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Comprueba que el numero sea correcto
     */
    private boolean checkNumber() {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(_APP)));
            _delay(this.delayValue);
            WebElement modal_opened = driver.findElement(By.cssSelector(_MODAL));
            if (modal_opened == null) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            return true;
        }
    }

    /**
     * Retorna la url de whatsapp formada con el numero de telefono y el mensaje
     */
    private String WHATSAPP_MESSAGE(String number, String message) {
        return "https://web.whatsapp.com/send?phone=" + number + "&text=" + message;
    }

    /**
     * Retardo en segundos
     */
    private void _delay(int seconds) {
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    private String getCodeQRImage(WebElement QR) {
        File scrFile = ((TakesScreenshot) QR).getScreenshotAs(OutputType.FILE);

        try {
            String tmpPath = scrFile.toURL().toString();
            return tmpPath.substring(tmpPath.indexOf('/') + 1, tmpPath.length());
        } catch (MalformedURLException ex) {
            return null;
        }

    }

    public void reload() {
        driver.navigate().refresh();
    }

    public void quit() {
        driver.quit();
    }

}
