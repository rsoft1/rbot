package rbot.view.utils;

import javax.swing.table.DefaultTableModel;

public class TableModel extends DefaultTableModel {

    public String[] titles;
    public Object[][] data;

    public TableModel(String[] titles, Object[][] data) {
        super();
        this.titles = titles;
        this.data = data;
        setDataVector(data, titles);
    }

    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
