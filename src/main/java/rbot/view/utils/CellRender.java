package rbot.view.utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CellRender extends DefaultTableCellRenderer {

    public CellRender() {
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {

        //Cells are by default rendered as a JLabel.
        JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, selected, focused, row, column);

        l.setHorizontalAlignment(JLabel.CENTER);
        l.setFont(new Font("Century Gothic", 14, 14));
        
        //si el tipo es icono entonces valida cual icono asignar a la etiqueta.
        switch ((String) value) {
            case "Bloqueado":
                l.setForeground(Color.ORANGE); 
                break;
            case "Activo":
                l.setForeground(Color.GREEN);
                break;
            case "Enviado":
                l.setForeground(Color.GREEN);
                break;
            case "Enviando":
                l.setForeground(Color.BLUE);
                break;
            case "Error":
                l.setForeground(Color.RED);
                break;
            default:
                l.setForeground(Color.BLACK);
        }

        return l;

    }

}
