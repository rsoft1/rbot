package rbot.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.LayoutManager;

/**
 *
 * @author Antabla
 */
public class MainView extends javax.swing.JFrame {

    private int mousePressedX = 0;
    private int mousePressedY = 0;

    public int select_row = -1;

    public MainView() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addContactDialog = new javax.swing.JDialog();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtIndicative = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtNumber = new javax.swing.JTextField();
        checkDefaultMessage = new javax.swing.JCheckBox();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtMessage = new javax.swing.JTextArea();
        jLabel11 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        navbarPane = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnClose = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnBot = new javax.swing.JButton();
        btnConfig = new javax.swing.JButton();
        btnReports = new javax.swing.JButton();
        btnHome = new javax.swing.JButton();
        layeredPane = new javax.swing.JLayeredPane();
        mainPane = new javax.swing.JPanel();
        QR = new javax.swing.JLabel();
        labelState = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableContacts = new javax.swing.JTable();
        btnDiffusion = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnLock = new javax.swing.JButton();
        btnSend = new javax.swing.JButton();
        botPane = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        reportsPane = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        btnDelete1 = new javax.swing.JButton();
        btnDelete2 = new javax.swing.JButton();
        btnDelete3 = new javax.swing.JButton();
        configPane = new javax.swing.JPanel();
        txtRetardo = new javax.swing.JLabel();
        txtDelay = new javax.swing.JSpinner();
        txtRetardo1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtMessageDefault = new javax.swing.JTextArea();
        btnSaveConfig = new javax.swing.JButton();
        loginPane = new javax.swing.JPanel();
        txtUsername = new javax.swing.JTextField();
        labelLoader2 = new javax.swing.JLabel();
        btnIngresar = new javax.swing.JButton();
        labelLoader3 = new javax.swing.JLabel();
        labelErrorRequest = new javax.swing.JLabel();
        txtPassword = new javax.swing.JPasswordField();
        labelLoader6 = new javax.swing.JLabel();
        labelLoader7 = new javax.swing.JLabel();
        labelSendRequest = new javax.swing.JLabel();
        loaderPane = new javax.swing.JPanel();
        labelLoader = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        addContactDialog.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addContactDialog.setBackground(new java.awt.Color(255, 255, 255));
        addContactDialog.setMinimumSize(new java.awt.Dimension(416, 418));
        addContactDialog.setResizable(false);
        addContactDialog.setSize(new java.awt.Dimension(416, 418));

        jPanel1.setBackground(new java.awt.Color(232, 232, 232));
        jPanel1.setMaximumSize(new java.awt.Dimension(416, 418));
        jPanel1.setMinimumSize(new java.awt.Dimension(416, 418));
        jPanel1.setPreferredSize(new java.awt.Dimension(416, 418));

        jLabel6.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Nuevo contacto");

        txtIndicative.setBackground(new java.awt.Color(255, 255, 255));
        txtIndicative.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        txtIndicative.setToolTipText("");

        jLabel7.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Mensaje:");

        txtNumber.setBackground(new java.awt.Color(255, 255, 255));
        txtNumber.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        txtNumber.setForeground(new java.awt.Color(0, 0, 0));

        checkDefaultMessage.setBackground(new java.awt.Color(232, 232, 232));
        checkDefaultMessage.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        checkDefaultMessage.setForeground(new java.awt.Color(0, 0, 0));
        checkDefaultMessage.setText("Usar mensaje por defecto");

        jLabel10.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Numero:");

        txtMessage.setBackground(new java.awt.Color(255, 255, 255));
        txtMessage.setColumns(20);
        txtMessage.setForeground(new java.awt.Color(0, 0, 0));
        txtMessage.setRows(5);
        jScrollPane2.setViewportView(txtMessage);

        jLabel11.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Indicativo:");

        btnSave.setBackground(new java.awt.Color(0, 102, 204));
        btnSave.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(255, 255, 255));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save.png"))); // NOI18N
        btnSave.setText("Guardar");
        btnSave.setBorder(null);
        btnSave.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtIndicative, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txtNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(checkDefaultMessage))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(156, 156, 156))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel6)
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIndicative, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(checkDefaultMessage)
                .addGap(18, 18, 18)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout addContactDialogLayout = new javax.swing.GroupLayout(addContactDialog.getContentPane());
        addContactDialog.getContentPane().setLayout(addContactDialogLayout);
        addContactDialogLayout.setHorizontalGroup(
            addContactDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        addContactDialogLayout.setVerticalGroup(
            addContactDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addContactDialogLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        navbarPane.setBackground(new java.awt.Color(55, 203, 55));
        navbarPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        navbarPane.setMaximumSize(new java.awt.Dimension(1000, 77));
        navbarPane.setMinimumSize(new java.awt.Dimension(1000, 77));
        navbarPane.setPreferredSize(new java.awt.Dimension(1000, 77));
        navbarPane.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                navbarPaneMouseDragged(evt);
            }
        });
        navbarPane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                navbarPaneMousePressed(evt);
            }
        });
        navbarPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/rs.png"))); // NOI18N
        navbarPane.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, -3, -1, 80));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Bot");
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        navbarPane.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 15, 60, 60));

        btnClose.setBackground(new java.awt.Color(55, 203, 55));
        btnClose.setForeground(new java.awt.Color(41, 155, 41));
        btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/turn-off.png"))); // NOI18N
        btnClose.setToolTipText("Salir");
        btnClose.setBorder(null);
        btnClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        navbarPane.add(btnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 10, 60, 60));

        btnRefresh.setBackground(new java.awt.Color(55, 203, 55));
        btnRefresh.setForeground(new java.awt.Color(41, 155, 41));
        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/refreshs.png"))); // NOI18N
        btnRefresh.setToolTipText("Refrescar codigo");
        btnRefresh.setBorder(null);
        btnRefresh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        navbarPane.add(btnRefresh, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 10, -1, -1));

        btnBot.setBackground(new java.awt.Color(55, 203, 55));
        btnBot.setForeground(new java.awt.Color(41, 155, 41));
        btnBot.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bots.png"))); // NOI18N
        btnBot.setToolTipText("Ir al bot");
        btnBot.setBorder(null);
        btnBot.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        navbarPane.add(btnBot, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 10, -1, -1));

        btnConfig.setBackground(new java.awt.Color(55, 203, 55));
        btnConfig.setForeground(new java.awt.Color(41, 155, 41));
        btnConfig.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cogss.png"))); // NOI18N
        btnConfig.setToolTipText("Ir a configuracion");
        btnConfig.setBorder(null);
        btnConfig.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        navbarPane.add(btnConfig, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 10, -1, -1));

        btnReports.setBackground(new java.awt.Color(55, 203, 55));
        btnReports.setForeground(new java.awt.Color(41, 155, 41));
        btnReports.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bars.png"))); // NOI18N
        btnReports.setToolTipText("Reportes y Estadisticas");
        btnReports.setBorder(null);
        btnReports.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        navbarPane.add(btnReports, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 10, -1, -1));

        btnHome.setBackground(new java.awt.Color(55, 203, 55));
        btnHome.setForeground(new java.awt.Color(41, 155, 41));
        btnHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/home.png"))); // NOI18N
        btnHome.setToolTipText("Ir a inicio");
        btnHome.setBorder(null);
        btnHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        navbarPane.add(btnHome, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 10, -1, -1));

        getContentPane().add(navbarPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, -1));

        layeredPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        layeredPane.setMaximumSize(new java.awt.Dimension(1000, 520));
        layeredPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        mainPane.setBackground(new java.awt.Color(242, 242, 242));
        mainPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        mainPane.setMaximumSize(new java.awt.Dimension(1000, 520));
        mainPane.setMinimumSize(new java.awt.Dimension(1000, 520));
        mainPane.setPreferredSize(new java.awt.Dimension(1000, 410));
        mainPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        QR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/qr_vacio.png"))); // NOI18N
        QR.setToolTipText("Codigo QR");
        QR.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mainPane.add(QR, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 80, -1, -1));

        labelState.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        labelState.setForeground(new java.awt.Color(255, 0, 0));
        labelState.setText("Offline");
        mainPane.add(labelState, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 30, -1, -1));

        jLabel8.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Lista de contactos");
        mainPane.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, -1));

        jLabel9.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Estado:");
        mainPane.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 30, -1, -1));

        tableContacts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Estado", "Numero", "Mensaje"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableContacts.setFocusable(false);
        tableContacts.setIntercellSpacing(new java.awt.Dimension(0, 0));
        tableContacts.setRowHeight(25);
        tableContacts.setSelectionBackground(new java.awt.Color(0, 153, 255));
        tableContacts.setSelectionForeground(new java.awt.Color(255, 255, 255));
        tableContacts.setShowVerticalLines(false);
        tableContacts.getTableHeader().setReorderingAllowed(false);
        tableContacts.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableContactsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableContacts);

        mainPane.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 670, 310));

        btnDiffusion.setBackground(new java.awt.Color(55, 203, 55));
        btnDiffusion.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnDiffusion.setForeground(new java.awt.Color(255, 255, 255));
        btnDiffusion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/sends.png"))); // NOI18N
        btnDiffusion.setText("Enviar");
        btnDiffusion.setToolTipText("Ir al bot");
        btnDiffusion.setBorder(null);
        btnDiffusion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDiffusion.setFocusable(false);
        mainPane.add(btnDiffusion, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 360, 120, 50));

        btnAdd.setBackground(new java.awt.Color(226, 226, 226));
        btnAdd.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/plus.png"))); // NOI18N
        btnAdd.setToolTipText("Nuevo contacto");
        btnAdd.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(153, 153, 153)));
        btnAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdd.setFocusable(false);
        mainPane.add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 20, 35, 40));

        btnEdit.setBackground(new java.awt.Color(226, 226, 226));
        btnEdit.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/edit.png"))); // NOI18N
        btnEdit.setToolTipText("Editar contacto");
        btnEdit.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(153, 153, 153)));
        btnEdit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEdit.setFocusable(false);
        mainPane.add(btnEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 20, 35, 40));

        btnDelete.setBackground(new java.awt.Color(226, 226, 226));
        btnDelete.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/rubbish.png"))); // NOI18N
        btnDelete.setToolTipText("Eliminar contacto");
        btnDelete.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(153, 153, 153)));
        btnDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete.setFocusable(false);
        mainPane.add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 20, 35, 40));

        btnLock.setBackground(new java.awt.Color(226, 226, 226));
        btnLock.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnLock.setForeground(new java.awt.Color(255, 255, 255));
        btnLock.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/lock.png"))); // NOI18N
        btnLock.setToolTipText("Bloquear / Desbloquear");
        btnLock.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(153, 153, 153)));
        btnLock.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLock.setFocusable(false);
        mainPane.add(btnLock, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 20, 35, 40));

        btnSend.setBackground(new java.awt.Color(226, 226, 226));
        btnSend.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnSend.setForeground(new java.awt.Color(255, 255, 255));
        btnSend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/sendss.png"))); // NOI18N
        btnSend.setToolTipText("Editar contacto");
        btnSend.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(153, 153, 153)));
        btnSend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSend.setFocusable(false);
        mainPane.add(btnSend, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 20, 35, 40));

        layeredPane.add(mainPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 1000, 450));

        botPane.setBackground(new java.awt.Color(242, 242, 242));
        botPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        botPane.setPreferredSize(new java.awt.Dimension(1000, 410));
        botPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel20.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(153, 153, 153));
        jLabel20.setText("No hay nada aqui");
        botPane.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 160, -1, -1));

        layeredPane.add(botPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 1000, 450));

        reportsPane.setBackground(new java.awt.Color(242, 242, 242));
        reportsPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        reportsPane.setPreferredSize(new java.awt.Dimension(1000, 410));
        reportsPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("No enviados:");
        reportsPane.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 160, -1, -1));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jTable1);

        reportsPane.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, 330));

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane5.setViewportView(jTextArea1);

        reportsPane.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 200, 460, 210));

        jLabel13.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("Historial");
        reportsPane.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        jLabel14.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("Detalles");
        reportsPane.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 40, -1, -1));

        jLabel15.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 0, 0));
        jLabel15.setText("100000000");
        reportsPane.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 160, -1, -1));

        jLabel16.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("Enviados:");
        reportsPane.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 120, -1, -1));

        jLabel17.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(0, 0, 0));
        jLabel17.setText("Total:");
        reportsPane.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 50, -1, -1));

        jLabel18.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("100000000");
        reportsPane.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 80, -1, -1));

        jLabel19.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(0, 204, 0));
        jLabel19.setText("100000000");
        reportsPane.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 120, -1, -1));

        btnDelete1.setBackground(new java.awt.Color(226, 226, 226));
        btnDelete1.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnDelete1.setForeground(new java.awt.Color(0, 0, 0));
        btnDelete1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/pdf.png"))); // NOI18N
        btnDelete1.setText("Exportar");
        btnDelete1.setToolTipText("Exportar en pdf");
        btnDelete1.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(153, 153, 153)));
        btnDelete1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete1.setFocusable(false);
        reportsPane.add(btnDelete1, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 130, 140, 40));

        btnDelete2.setBackground(new java.awt.Color(226, 226, 226));
        btnDelete2.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnDelete2.setForeground(new java.awt.Color(0, 0, 0));
        btnDelete2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/rubbish.png"))); // NOI18N
        btnDelete2.setText("Borrar todo");
        btnDelete2.setToolTipText("Borrar historial");
        btnDelete2.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(153, 153, 153)));
        btnDelete2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete2.setFocusable(false);
        reportsPane.add(btnDelete2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 40, 140, 36));

        btnDelete3.setBackground(new java.awt.Color(226, 226, 226));
        btnDelete3.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnDelete3.setForeground(new java.awt.Color(0, 0, 0));
        btnDelete3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/excel.png"))); // NOI18N
        btnDelete3.setText("Exportar");
        btnDelete3.setToolTipText("Exportar en excel");
        btnDelete3.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 2, 0, new java.awt.Color(153, 153, 153)));
        btnDelete3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete3.setFocusable(false);
        reportsPane.add(btnDelete3, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 80, 140, 40));

        layeredPane.add(reportsPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 1000, 450));

        configPane.setBackground(new java.awt.Color(242, 242, 242));
        configPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        configPane.setPreferredSize(new java.awt.Dimension(1000, 410));
        configPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtRetardo.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        txtRetardo.setForeground(new java.awt.Color(0, 0, 0));
        txtRetardo.setText("Mensaje por defecto");
        configPane.add(txtRetardo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, -1, -1));

        txtDelay.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        configPane.add(txtDelay, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 60, -1));

        txtRetardo1.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        txtRetardo1.setForeground(new java.awt.Color(0, 0, 0));
        txtRetardo1.setText("Retardo:");
        configPane.add(txtRetardo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        jScrollPane3.setForeground(new java.awt.Color(0, 0, 0));

        txtMessageDefault.setColumns(20);
        txtMessageDefault.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        txtMessageDefault.setForeground(new java.awt.Color(0, 0, 0));
        txtMessageDefault.setRows(5);
        jScrollPane3.setViewportView(txtMessageDefault);

        configPane.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 460, 210));

        btnSaveConfig.setBackground(new java.awt.Color(55, 203, 55));
        btnSaveConfig.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnSaveConfig.setForeground(new java.awt.Color(255, 255, 255));
        btnSaveConfig.setText("Guardar");
        configPane.add(btnSaveConfig, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 360, -1, -1));

        layeredPane.add(configPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 1000, 450));

        loginPane.setBackground(new java.awt.Color(242, 242, 242));
        loginPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        loginPane.setMaximumSize(new java.awt.Dimension(1000, 520));
        loginPane.setMinimumSize(new java.awt.Dimension(1000, 520));
        loginPane.setPreferredSize(new java.awt.Dimension(1000, 520));
        loginPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtUsername.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        loginPane.add(txtUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 220, 240, -1));

        labelLoader2.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        labelLoader2.setForeground(new java.awt.Color(0, 0, 0));
        labelLoader2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        labelLoader2.setText("Contraseña");
        labelLoader2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loginPane.add(labelLoader2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 280, 270, -1));

        btnIngresar.setBackground(new java.awt.Color(55, 203, 55));
        btnIngresar.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btnIngresar.setForeground(new java.awt.Color(255, 255, 255));
        btnIngresar.setText("INGRESAR");
        loginPane.add(btnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 390, -1, -1));

        labelLoader3.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        labelLoader3.setForeground(new java.awt.Color(0, 0, 0));
        labelLoader3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        labelLoader3.setText("Nombre de usuario");
        labelLoader3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loginPane.add(labelLoader3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 180, 270, -1));

        labelErrorRequest.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelErrorRequest.setForeground(new java.awt.Color(204, 0, 0));
        labelErrorRequest.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelErrorRequest.setText("Credenciales invalidas");
        labelErrorRequest.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loginPane.add(labelErrorRequest, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 180, 250, 190));

        txtPassword.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        loginPane.add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 320, 240, -1));

        labelLoader6.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        labelLoader6.setForeground(new java.awt.Color(0, 0, 0));
        labelLoader6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelLoader6.setText("BIENVENIDO");
        labelLoader6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loginPane.add(labelLoader6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 280, -1));

        labelLoader7.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        labelLoader7.setForeground(new java.awt.Color(0, 0, 0));
        labelLoader7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelLoader7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bot.png"))); // NOI18N
        labelLoader7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loginPane.add(labelLoader7, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 120, 340, -1));

        labelSendRequest.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelSendRequest.setForeground(new java.awt.Color(204, 0, 0));
        labelSendRequest.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelSendRequest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/sendrequest.gif"))); // NOI18N
        labelSendRequest.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loginPane.add(labelSendRequest, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 190, 250, 190));

        layeredPane.add(loginPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 520));

        loaderPane.setBackground(new java.awt.Color(55, 203, 55));
        loaderPane.setMaximumSize(new java.awt.Dimension(1000, 520));
        loaderPane.setMinimumSize(new java.awt.Dimension(1000, 520));
        loaderPane.setPreferredSize(new java.awt.Dimension(1000, 520));
        loaderPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labelLoader.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        labelLoader.setForeground(new java.awt.Color(255, 255, 255));
        labelLoader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelLoader.setText("CARGANDO..");
        labelLoader.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loaderPane.add(labelLoader, new org.netbeans.lib.awtextra.AbsoluteConstraints(-2, 330, 1000, -1));

        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/loader.gif"))); // NOI18N
        loaderPane.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 100, -1, -1));

        layeredPane.add(loaderPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 520));

        getContentPane().add(layeredPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 520));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Eventos que nos permiten arrastrar la ventana por pantalla
     */
    private void navbarPaneMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_navbarPaneMouseDragged
        int kordinatX = evt.getXOnScreen();
        int kordinatY = evt.getYOnScreen();

        this.setLocation(kordinatX - mousePressedX, kordinatY - mousePressedY);
    }//GEN-LAST:event_navbarPaneMouseDragged

    private void navbarPaneMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_navbarPaneMousePressed
        this.mousePressedX = evt.getX();
        this.mousePressedY = evt.getY();

    }//GEN-LAST:event_navbarPaneMousePressed

    private void tableContactsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableContactsMouseClicked
        select_row = tableContacts.rowAtPoint(evt.getPoint());
    }//GEN-LAST:event_tableContactsMouseClicked

    /**
     * Funcion que nos permite cambiar de ventana y ocultar algunos botones
     * segun sea el caso
     *
     * @param pane
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel QR;
    public javax.swing.JDialog addContactDialog;
    public javax.swing.JPanel botPane;
    public javax.swing.JButton btnAdd;
    public javax.swing.JButton btnBot;
    public javax.swing.JButton btnClose;
    public javax.swing.JButton btnConfig;
    public javax.swing.JButton btnDelete;
    public javax.swing.JButton btnDelete1;
    public javax.swing.JButton btnDelete2;
    public javax.swing.JButton btnDelete3;
    public javax.swing.JButton btnDiffusion;
    public javax.swing.JButton btnEdit;
    public javax.swing.JButton btnHome;
    public javax.swing.JButton btnIngresar;
    public javax.swing.JButton btnLock;
    public javax.swing.JButton btnRefresh;
    public javax.swing.JButton btnReports;
    public javax.swing.JButton btnSave;
    public javax.swing.JButton btnSaveConfig;
    public javax.swing.JButton btnSend;
    public javax.swing.JCheckBox checkDefaultMessage;
    public javax.swing.JPanel configPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    public javax.swing.JLabel labelErrorRequest;
    public javax.swing.JLabel labelLoader;
    public javax.swing.JLabel labelLoader2;
    public javax.swing.JLabel labelLoader3;
    public javax.swing.JLabel labelLoader6;
    public javax.swing.JLabel labelLoader7;
    public javax.swing.JLabel labelSendRequest;
    public javax.swing.JLabel labelState;
    public javax.swing.JLayeredPane layeredPane;
    public javax.swing.JPanel loaderPane;
    public javax.swing.JPanel loginPane;
    public javax.swing.JPanel mainPane;
    private javax.swing.JPanel navbarPane;
    public javax.swing.JPanel reportsPane;
    public javax.swing.JTable tableContacts;
    public javax.swing.JSpinner txtDelay;
    public javax.swing.JTextField txtIndicative;
    public javax.swing.JTextArea txtMessage;
    public javax.swing.JTextArea txtMessageDefault;
    public javax.swing.JTextField txtNumber;
    public javax.swing.JPasswordField txtPassword;
    private javax.swing.JLabel txtRetardo;
    private javax.swing.JLabel txtRetardo1;
    public javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
